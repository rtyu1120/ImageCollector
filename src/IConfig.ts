export interface IConfig {
    [source: string]: ISourceConfig;
    
}
export interface ISourceConfig {
    params: any[];
    identifier: any[];
} // Oops, no type for each source params