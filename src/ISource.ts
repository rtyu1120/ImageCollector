import { IImage } from './IImage'
export interface ISource {
    readonly type: Source;
    import(id: any, callback: IImageReciverFunc): void;
}
export interface IImageReciverFunc {
    (image: IImage): void;
}
export enum Source {
    FOURCHAN = '4chan', MASTODON = 'mastodon', REDDIT = 'reddit'
}