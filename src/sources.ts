import { ISource, IImageReciverFunc, Source } from './ISource';
import { IImage } from './IImage';
import * as Promise from 'bluebird';
import * as rp from 'request-promise';
import * as request from 'request';

// 4chan
import * as chan from '4chanjs';
export class Chan implements ISource {
    type = Source.FOURCHAN
    constructor (private boardName: ChanBoard = ChanBoard.C, private domain: string = 'boards.4chan.org') { }
    async import (id: number, callback: IImageReciverFunc) {
        let board = Promise.promisifyAll(chan.board(this.boardName))
        let posts = await board.thread(id)
        let urls: string[] = posts.map(post => `https://i.4cdn.org/${boardName}/${post.tim}.${post.ext}`)
        let images: IImage[] = []

        urls.forEach(url => {
            rp(url)
                .then(body => {
                    let image = Buffer.from(body, 'binary')
                    callback({
                        ext: url.slice(-4),
                        image: image
                    })
                })
                .catch(console.error)
        });
    }
}
export enum ChanBoard {
    C = 'c', B = 'b', A = 'a', W = 'w'
}

// Mastodon Hashtag
import * as EventSource from 'eventsource';
export class Mastodon implements ISource {
    type = Source.MASTODON
    constructor (private domain: string) {}
    import (hashtag: string, callback: IImageReciverFunc) {
        let mastodon = new EventSource(`https://${domain}/api/v1/streaming/hashtag?tag=${hashtag}`)
        mastodon.addEventListener('update', (e: IMastodonUpdate) => {
            let urls: string[] =  e.data.media_attachments.filter(a => a.type === 'image').map(i => i.url)
            urls.forEach(url => {
                rp(url)
                    .then(body => {
                        let image = Buffer.from(body, 'binary')
                        callback({
                            ext: url.slice(-4),
                            image: image
                        })
                    })
                    .catch(console.error)
            });
        })
    }
}
export interface IMastodonUpdate {
    event: string;
    data: any; // TODO: define https://github.com/tootsuite/documentation/blob/master/Using-the-API/API.md#status
}

// Reddit
import * as snoowrap from 'snoowrap'
export class Reddit implements ISource {
    type = Source.REDDIT
    constructor (private options: snoowrap.SnoowrapOptions) {} // subreddit should not contain slash
    import (subreddit: string, callback: IImageReciverFunc) {
        let r = new snoowrap(this.options)
        let urls = r.getSubreddit(subreddit).getHot().fetchAll().map(p => p.url)
        urls.forEach(url => {
            rp({ uri: url, resolveWithFullResponse: true })
                .then((res: request.Response) => {
                    if(!res.headers["content-type"].startsWith('image/')) return
                    let image = Buffer.from(res.body, 'binary')
                    callback({
                        ext: url.slice(-4),
                        image: image
                    })
                })
                .catch(console.error)
        })
    }
}