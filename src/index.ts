import * as Sources from './sources';
import * as config from 'config';
import { Source } from './ISource';
import { ISourceConfig } from './IConfig';
import { IImage } from './IImage';

function reciver(image: IImage): void {

}

var sources: string[] = []

for(var n in Source) {
    if (typeof Source[n] === 'string') {
        let conf: ISourceConfig = config.get(Source[n]);
        switch(<Source>Source[n]) {
            case Source.FOURCHAN:
                let chan = new Sources.Chan(conf.params[0], conf.params[1])
                conf.identifier.forEach(id => chan.import(id, reciver))
            case Source.MASTODON:
                let mstdn = new Sources.Mastodon(conf.params[0])
                conf.identifier.forEach(hashtag => mstdn.import(hashtag, reciver))
            case Source.REDDIT:
                let reddit = new Sources.Reddit(conf.params[0])
                conf.identifier.forEach(subreddit => reddit.import(subreddit, reciver))
        }
    }
}