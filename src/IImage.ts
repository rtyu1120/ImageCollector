export interface IImage {
    ext: string; // including a dot
    image: Buffer;
}